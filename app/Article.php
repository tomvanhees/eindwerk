<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Cviebrock\EloquentTaggable\Taggable;

class Article extends Model
{
    protected $fillable = ['user_id', 'title', 'content', 'date'];

    use CanBeLiked;
    use Taggable;


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }


}
