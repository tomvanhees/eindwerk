<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profilepicture extends Model
{
    protected $fillable = ['profile_id', 'picturepath'];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }
}
