<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profilepicture;
use Laravel\Scout\Searchable;
use App\Header;
use Illuminate\Support\Facades\Storage;

class Profile extends Model
{
    use Searchable;
    protected $fillable = ['user_id', 'profilepicture_id', 'gender', 'birthdate', 'facebook_profile_url'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getProfilePicture()
    {

        $path = Profilepicture::find($this->profilepicture_id)['picturepath'];


        if (substr($path, 0, 6) === 'public') {
            return Storage::url($path);
        } else {
            return $path;
        }

    }

    public function getProfileHeader()
    {
        return Header::find($this->header_id)['path'];
    }


    public function profilepicture()
    {
        return $this->hasMany('App\Profilepicture');
    }

    public function header()
    {
        return $this->hasMany('App\Header');
    }
}
