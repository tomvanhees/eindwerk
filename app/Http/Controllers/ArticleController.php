<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Notifications\LikeArticle;

class ArticleController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAllArticles()
    {
        $articles = Article::whereIn('user_id', function ($query) {
            $query->select('followable_id')
                ->from('followables')
                ->where('user_id', Auth::id())
                ->where('followable_type', 'App\User');
        })->orWhere('user_id', Auth::id())->get()->reverse();

        $users = $this->getUsers();

        return view('frontpage.startpagina', compact('articles', 'users'));
    }

    private function getUsers()
    {
        //First find all friends
        $allFriends = User::join('followables', 'users.id', '=', 'followables.user_id')->where('followable_type', 'App\User')->where('id', Auth::id())->get();
        if ($allFriends->isEmpty()) {
            $parameters = [Auth::id()];
        } else {
            $parameters = [Auth::id(), $allFriends];
        }

        //Then search every user and exclude all friends and current user
        return User::where('active', 1)->whereNotIn('id', $parameters)->inRandomOrder()->take(5)->get();

    }


    public function getAllArticlesByUserId($id)
    {
        $articles = Article::with('user')->where('user_id', '=', $id)->get()->reverse();
        $pageowner = User::find($id);
        return view('frontpage.userpagina', compact('articles', 'pageowner'));

    }

    public function showArticle($articleId)
    {

        $articles = Article::where('id', $articleId)->get();
        $pageowner = Auth::user();
        return view('frontpage.userpagina', compact('articles', 'pageowner'));
    }

    public function toggleLike($user_id, $article_id)
    {
        $user = Auth::user();
        $user->toggleLike($article_id, Article::class);
        $user->save();

        $article_user = Article::find($article_id)->user;

        if ($article_user->id != Auth::id()) {
            $article_user->notify(new LikeArticle(Auth::id(), $article_id));
        }

        return redirect()->back();
    }

    public function tag($tag)
    {
        $articles = Article::withAllTags($tag)->get();
        $users = $this->getUsers();
        return view('frontpage.startpagina', compact('articles', 'users'));
    }

    public function article()
    {
        $user = Auth::user();
        $pageowner = Auth::user();
        return view('user.article', compact('user', 'pageowner'));
    }

    public function postArticle(Request $request)
    {

        if ($request['content'] != null) {
            $article = new Article();
            $article->user_id = Auth::id();
            if ($request->content != "") {
                $article->content = $request->content;
            }
            if ($request->file('image') != null) {
                $article->picturepath = $request->file('image')->store('public/users/' . $article->user_id . '/article');
            }
            $article->save();
            if ($request->tags != '') {

                $article->tag($request->tags);
                $article->save();
            }


        };


        return redirect('/');

    }
}
