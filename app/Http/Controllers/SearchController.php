<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class SearchController extends Controller
{
    public function search(Request $request)
    {
        $search = User::search($request->search)->get();

        return view('frontpage.search', compact('search'));
    }
}
