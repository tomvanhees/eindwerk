<?php

namespace App\Http\Controllers;

use App\Article;
use App\Notifications\CommentAdded;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\Notifications\LikeComment;

class CommentController extends Controller
{


    public function toggleLike($user_id, $comment_id, $article_id)
    {

        $user = Auth::user();
        $user->toggleLike($comment_id, Comment::class);
        $user->save();

        $comment_user = Comment::find($comment_id)->user;

        if ($comment_user->id != Auth::id()) {
            $comment_user->notify(new LikeComment(Auth::id(), $article_id));
        }
        return redirect()->back();
    }

    public function addComment($article_id, Request $request)
    {
        $comment = new Comment;
        $comment->user_id = Auth::id();
        $comment->article_id = $article_id;
        $comment->content = $request->comment;
        $comment->save();


        $article = Article::find($article_id);

        if ($article->id != Auth::id()) {
            $article->user->notify(new CommentAdded($comment));
        }


        return redirect()->back();
    }

}
