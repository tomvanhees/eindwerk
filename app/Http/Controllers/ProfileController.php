<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUser;
use Illuminate\Http\Request;
use App\Profile;
use App\Profilepicture;
use Illuminate\Support\Facades\Auth;
use App\Header;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('notifications');
    }

    public function user()
    {
        $user = Auth::user();
        $pageowner = $user;
        return view('user.edituser', compact('user', 'pageowner'));
    }


    public function editUser(UpdateUser $request)
    {

        $user = Auth::user();
        $user->name = $request->name;
        $user->save();


        if ($request->file('image') != null) {
            $profilepicture = new Profilepicture();
            $profilepicture->profile_id = $user->id;
            $profilepicture->picturepath = $request->file('image')->store('public/users/' . $user->id . '/profiles');
            $profilepicture->save();
        }


        $profile = Profile::where('user_id', $user->id)->first();
        $profile->birthdate = $request->birthdate;
        $profile->gender = $request->gender;

        if ($request->file('image') != null) {
            $profile->profilepicture_id = $profilepicture->id;
        }

        $profile->save();

        return redirect()->back();
    }


    public function toggleFollowUser($id)
    {
        $user = Auth::user();
        $user->toggleFollow($id);
        $user->save();
        return redirect()->back();
    }


    public function photo()
    {
        $user = Auth::user();
        $pageowner = $user;
        $defaultpictures = Profilepicture::where('profile_id', '1')->get();
        return view('user.editphoto', compact('user', 'pageowner', 'defaultpictures'));
    }

    public function editPhoto($id, Request $request)
    {
        //Is it a default picture?
        $profilepicture = Profilepicture::whereBetween('id', ['1', '9'])->where('id', $id)->get()->first();

        if ($profilepicture != null) {
            $profile = Profile::find(Auth::id());
            $profile->profilepicture_id = $id;
            $profile->save();
            return redirect()->back();
        }

        /* Not a default picture => check if user and picture already exist
         *
         * Else make a new entry
         * */

        $profilepicture = Profilepicture::where('profile_id', Auth::id())->where('id', $id)->get();

        if ($profilepicture->isNotEmpty()) {
            $profile = Profile::where('user_id', Auth::id())->get()->first();
            $profile->profilepicture_id = $id;
            $profile->save();
            return redirect()->back();
        }
        return redirect()->back();

    }

    public function addPhoto(Request $request)
    {

        if ($request->file('image') != null) {
            $profilepicture = new Profilepicture();
            $profilepicture->profile_id = Auth::id();
            $profilepicture->picturepath = $request->file('image')->store('public/' . Auth::id() . '/profiles');
            $profilepicture->save();
        }
        return redirect()->back();
    }

    public function header()
    {
        $headers = Header::all();
        $pageowner = Auth::user();
        return view('user.header', compact('headers', 'pageowner'));
    }

    public function editHeader($id)
    {
        $profile = Profile::find(Auth::id());
        $profile->header_id = $id;
        $profile->save();
        return redirect()->back();
    }

    public function notifications()
    {
        $pageowner = Auth::user();
        return view('user.notifications', compact('pageowner'));
    }


}




/*$user->profile = new Profile
$user->profile->img = 'img/default.jpg';

@if($user->profile->img)*/
