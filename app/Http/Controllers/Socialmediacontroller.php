<?php

namespace App\Http\Controllers;

use App\Profilepicture;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Auth;

class Socialmediacontroller extends Controller
{

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function handleProviderCallback()
    {
        $facebook = Socialite::driver('facebook')->user();

        $user = User::where('facebook_id', $facebook->id)->get()->first();


        if (!is_object($user)) {
            $user = User::create([
                'name' => $facebook->name,
                'email' => $facebook->email,
                'password' => bcrypt(str_random(16)),
                'facebook_id' => $facebook->id
            ]);

            if ($user->gender === "male") {
                $gender = 0;
            } else {
                $gender = 1;
            }

            $profilepicture = new Profilepicture;
            $profilepicture->profile_id = $user->id;
            $profilepicture->picturepath = $facebook->getAvatar();
            $profilepicture->save();

            $profile = new Profile();
            $profile->user_id = $user->id;
            $profile->profilepicture_id = $profilepicture->id;
            $profile->header_id = 1;
            $profile->gender = $gender;
            $profile->facebook_profile_url = $facebook->profileUrl;
            $profile->save();
        }

        Auth::login($user);

        return redirect('/');


    }
}
