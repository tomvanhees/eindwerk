<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    protected $fillable = ['user_id', 'path'];

    public function header()
    {
        return $this->hasOne('App\Profile');
    }
}
