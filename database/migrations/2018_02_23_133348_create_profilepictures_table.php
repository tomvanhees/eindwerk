<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilepicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profilepictures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id');
            $table->string('picturepath')->default('public/default/users/MmKiCfviDEUsncGgV9YrczNoj7ctMuzPG88ATA7r.jpeg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profilepictures');
    }
}
