<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        Schema::enableForeignKeyConstraints();

        $voornamen = [
            'Tisa','Teisha','Alfredo','Georgeann','Almeta','Lin','Rona','Danae','Natalya','Marcos','Kathey','Enedina',
            'Kallie','Hilda','Roseann','Sheridan','Helene','Kortney','Rachal','Holly','Maud','Sheena','Deandrea','Chara',
            ' Keena ','Clora','Loise','Magdalene','Kara','Vena','Tamra','Kourtney','Eneida','Darci','Shenita','Tiny ',
            'Carley','Oneida','Eufemia','Fern','India','Twana','Gay','Candy','Bibi','Larisa','Bernarda','Providencia',
            'Terese','Curt'
        ];
            $achternamen = [
                'Feltner','Kentner','Stearns','Glymph','Hennessy','Hinnenkamp','Odaniel','Marlin','Loewen','Lafollette',
                'Pullum','Euell','Macken','Vergara','Turmelle','Frahm','Haberkorn','Paynter','Munson','Cort','Randall','Bove',
                'Powley','Bankes','Risko','Schacherer','Fountain','Rippeon','Linzy','Walch','Glaude','Ruppert','Amor','Morrone',
                'Esposito','Schwing','Kung','Morvant','Iskra','Sowder','Ancheta','Phillip','Brundige','Gunther','Alto',
                'Baeza','Shield','Pigford','Rimes','Hansel'
            ];


        $user = new User;
        $user->name = 'default';
        $user->email = str_random(5).'@test.be';
        $user->password = bcrypt('secret');
        $user->active = 0;
        $user->save();

        for ($i =0; $i < 10; $i++){
            $user = new User;
            $user->name = $voornamen[random_int(0,49)] . " ".  $achternamen[random_int(0,49)];
            $user->email = str_random(5).'@test.be';
            $user->password = bcrypt('secret');
            $user->save();
        }

    }
}
