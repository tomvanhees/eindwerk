<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Profilepicture;
use Illuminate\Http\File;

class ProfilepictureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('profilepictures')->truncate();
        Schema::enableForeignKeyConstraints();


        $defaultpictures = [
            'public/default/users/2evNypLSzdvyffHBRtmdKk2DxluCjcyFafF6PY6a.jpeg',
            'public/default/users/Aj4W1y0OaIvUKMkOTuYHkgIEJBtftqFspcUhn93t.jpeg',
            'public/default/users/G5aF1OO3disK5iR1pSTV8rrcBoZ3boEPMGWix5Ji.jpeg',
            'public/default/users/hLdoDUBATzE4aBTukH6udvqDdWrhuk3eF6pEgPju.jpeg',
            'public/default/users/lupUdMIldhDxQSip7RqnNs51kQ6E0YqhRsUngBEO.jpeg',
            'public/default/users/MmKiCfviDEUsncGgV9YrczNoj7ctMuzPG88ATA7r.jpeg',
            'public/default/users/nnAymLUkGJiRIpvInD3XXYLOFfJKRg4Cm269MDhl.jpeg',
            'public/default/users/UgLWNpj06e1HyMl6DoL7uqIIjfboAoftzmcThvsR.jpeg',
            'public/default/users/WnhjdJbG6JPj6nqI9dPbW9noz3zEOpbNhHuW54BS.jpeg'
        ];

        for($i=0; $i < count($defaultpictures); $i++){
            $profilepicture = new Profilepicture;
            $profilepicture->profile_id = 1;
            $profilepicture->picturepath =$defaultpictures[$i];
            $profilepicture->save();

        }
    }
}