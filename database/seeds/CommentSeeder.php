<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Article;
use App\Comment;


class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('comments')->truncate();
        Schema::enableForeignKeyConstraints();

        $users = User::all();
        $articles = Article::all();

        $source = 'Stella steigerde door de steigerende stukke stekkerstraat en vroeg de steigerende stukke stekker om steigerendestukkestekkersraad
Zeven scheve neven bleven even zweven Wat was was eer was was was? Eer was was was was was is Mijn moeder legt brood op de plank Was de wasmachine waar de was is was al gewassen?';
        $wordgenerator = explode(' ', $source);


        foreach ($articles as $article){
              for ($i=0; $i < random_int(0, 10); $i++){
                $zin ='';

                for($a=0; $a < random_int(0, 50); $a++ ){
                    $zin = $zin. ' '. $wordgenerator[random_int(0, count($wordgenerator)-1)] ;
                }

                $comment =new Comment;
                $comment->article_id = $article->id;
                $comment->user_id = random_int(1,count($users));
                $comment->content = $zin;
                $comment->save();
            }


        }


    }
}
