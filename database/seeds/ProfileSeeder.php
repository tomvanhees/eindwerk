<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Profile;
class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        DB::table('profiles')->truncate();
        Schema::enableForeignKeyConstraints();

        $users = User::all();


            $profile = new Profile;
            $profile->user_id = 1;
            $profile->profilepicture_id = 1;
            $profile->header_id = 1;
            $profile->gender = random_int(0, 1);
            $profile->birthdate = date("Y-m-d", mt_rand(1, time()));
            $profile->save();


        foreach ($users as $user){
            if($user->id != 1) {
                $profile = new Profile;
                $profile->user_id = $user->id;
                $profile->profilepicture_id = random_int(1,9);
                $profile->header_id = random_int(1,8);
                $profile->gender = random_int(0, 1);
                $profile->birthdate = date("Y-m-d", mt_rand(1, time()));
                $profile->save();
            }
    }
    }
}
