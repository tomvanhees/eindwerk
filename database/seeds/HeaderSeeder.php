<?php

use Illuminate\Database\Seeder;
use App\Header;
class HeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

        public function run()
    {
        $default = [
            'public/default/header/2drzDRMThPZVt2yKfp3nLerZgeTmhXlUCnG718yK.jpeg',
            'public/default/header/Aul1pm5mMlh5Kaqp8z1A9Wg6DLctqg7VY4FokTlg.jpeg',
            'public/default/header/f1pbOA2gVGz4KTuiuD55KDFncHruMuw7D6FO6dfV.jpeg',
            'public/default/header/gGn01G0KIyQlkvTeGne7ggBAspm9hDakeTVFTZC5.jpeg',
            'public/default/header/jSKo5aKHvn7LiUflVXxMaIQsKUSCzoUlzDs5E5gb.jpeg',
            'public/default/header/u5vqDGg6TXL7K6KViUOk8XBYURawAyzbk36WgZiq.jpeg',
            'public/default/header/xHplkSEFYHvHvPNksQous9zMCP2OKmvsl0z8xf1N.jpeg',
            'public/default/header/ZAj9W2sGf8V16TyVUbp9MVsrejofsCSnsI1ZQrM8.jpeg'
        ];


        Schema::disableForeignKeyConstraints();
        DB::table('headers')->truncate();
        Schema::enableForeignKeyConstraints();


        for($i=0;$i < 8;$i++){
            $profileheader = new Header;
            $profileheader->profile_id = 1;
            $profileheader->path = $default[$i];
            $profileheader->save();
        }


    }
    }

