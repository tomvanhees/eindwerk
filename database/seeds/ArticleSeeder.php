<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Article;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('articles')->truncate();
        Schema::enableForeignKeyConstraints();

        $users = User::all();

        for ($i=0; $i<20; $i++ ){
            $article = new Article;
            $article->user_id = random_int(1, count($users));
            $article->title = str_random(10);
            $article->content = str_random(10);
            $article->save();


        }

    }
}
