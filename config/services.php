<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'sandboxd54903c9e2904a24bcf981dde56ae371.mailgun.org',
        'secret' => 'key-5bff2e2ad57b5ceea648bab7bf7c3478',
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '933507130158368',         // Your GitHub Client ID
        'client_secret' => 'ea7732d04a89b5bd69aa4701acda6e3c', // Your GitHub Client Secret
        'redirect' => 'http://eindwerk.oo/login/facebook/callback',
    ],

];
