<div class="card mt-2">
    <div class="card-body bg-light border border-dark">
        <p><a href="{{ route('user.page', $article->user->id) }}"><img src="{{ $article->user->profile->getProfilePicture() }}" style=""
                                                                       alt=""
                                                                       class="rounded-circle mr-2 small_icon">{{ $article->user->name. ' '. $article->user->surname }}
            </a> deelt:</p>

        @isset($article->content)
            <div class="bg-white p-2"> {{ $article->content }}</div>
        @endisset

        @isset($article->picturepath)
            <img class="card-img-top" src="{{ Storage::url($article->picturepath) }}" alt="Card image cap">
        @endisset


        <div class="bg-white p-2">
            @foreach($article->tags as $tag)
                <a href="{{ route('tag',$tag->name) }}">#{{$tag->name}}</a>

            @endforeach
        </div>

        <div class="bg-dark p-2 d-flex justify-content-between rounded-bottom">
                <span class="font-weight-bold mr-1 text-white">Likes: <span>{{ $article->likers->count() }}</span>
                </span>
            <a href="{{ route('like.article',[$article->user->id,$article->id] ) }}" class="text-white">{{ $article->isLikedBy(Auth::id()) ? 'unlike' : 'like' }}</a>

        </div>

        <div class="card-body rounded">


            @foreach($article->comment as $comment)
                @include('partial.comment')
            @endforeach


            <div class="mt-3">
                <form action="{{ route('add.comment', $article->id) }}" method="post" class="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Plaats uw opmerking..." name="comment">

                        <button class="btn mt-1 btn-primary">Comment</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

