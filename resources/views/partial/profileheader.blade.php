<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="profileheader  position-relative d-flex align-content-center">

                @if( isset($pageowner))
                    <img src="{{ Storage::url( $pageowner->profile->getProfileHeader() ) }}" alt="" class="rounded">
                    <img src="{{ $pageowner->profile->getProfilePicture() }}" alt="" class="p-2 bg-white rounded profielfoto position-absolute">
                    @if($pageowner->id != Auth::id())
                        <a href="{{ route('follow',$pageowner->id) }}" class="text-white mt-2  position-absolute followbutton">{!! Auth::user()->isFollowing($pageowner->id) ? '<i class="fas fa-smile ml-3 fa-2x"></i><br>Followed' : '<i class="far fa-smile ml-2 fa-2x"></i><br>follow' !!}</a>
                    @endif
                @else
                    <img src="{{ Storage::url( Auth::user()->profile->getProfileHeader() ) }}" alt="" class="rounded">
                    <img src="{{  Auth::user()->profile->getProfilePicture() }}" alt="" class="p-2 bg-white rounded profielfoto position-absolute">
                @endif
            </div>
        </div>
    </div>
</div>