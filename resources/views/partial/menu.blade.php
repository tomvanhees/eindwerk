<nav class="navbar navbar-expand-lg fixed-top  navbar-dark bg-primary">
    <div class="container">
        <a href="/" class="text-white navbar-brand"><i class="fab fa-themeisle fa-2x mr-2"></i><span class="font-weight-bold">Parrotshare</span></a>


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">


            @if(Auth::check())
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a href="{{ route('user.page', Auth::id())  }}" class="text-white font-weight-bold small">
                            <img src="{{ Auth::user()->profile->getProfilePicture() }}" alt="{{ Auth::user()->name .'_'.Auth::user()->surname }}" class="small_icon rounded-circle ml-3">
                            <span class=" ">{{ Auth::user()->name .' '. Auth::user()->surname }}</span>
                        </a>
                    </li>
                    <li class="nav-item ml-2">
                        <a href="{{ route('home') }}" class="text-white font-weight-bold"><i class="fas fa-home"></i>Home</a>
                    </li>
                    <li class="nav-item ml-2">
                        <a href="{{ route('notifications') }}" class="text-white font-weight-bold ">
                            {!! Auth::user()->unReadNotifications->count()!= 0 ? '<i class="fas fa-flag"></i>'   : '<i class="far fa-flag"></i>' !!} ({{ Auth::user()->unReadNotifications->count() }})
                        </a>
                    </li>
                    <li class="nav-item ml-2">
                        <a href="{{ route('article') }}" class="text-white font-weight-bold">Share</a>
                    </li>
                    <li class="nav-item ml-2">
                        <a href="{{ route( 'logout') }}" class="text-white"><i class="fas fa-sign-out-alt"></i>Logout</a>
                    </li>
                </ul>


                <form action="{{ route('search') }}" method="get" class="form-inline">
                    <input type="search" name="search" class="form-control">
                    <button type="submit" class="btn btn-outline-light ml-2">Zoek</button>
                </form>

            @endif
        </div>
    </div>
</nav>

