<div class="row">
    <div class="col-sm-1">
        <a href="{{ route('user.page', $comment->user->id) }}"
           class="pl-1"><img src="{{ $comment->user->profile->getProfilePicture() }}"
                             alt=""
                             class="rounded-circle small_icon"></a>
    </div>
    <div class="col-sm-11">
        <div class="bg-white rounded pt-1 pb-1 mt-1 mb-1">
            <a href="{{ route('user.page', $comment->user->id) }}" class="pl-1">
                {{ $comment->user->name .' '. $comment->user->surname }}</a>
            <span class="ml-2">{{ $comment->content }}</span>

            <p class="d-flex justify-content-end mr-2 mt-2"><a href="{{ route('like.comment',[$comment->user->id, $comment->id, $article->id] ) }}">{{ $comment->isLikedBy(Auth::id()) ? 'unlike' : 'like' }}</a></p>
        </div>
    </div>
</div>