@if(Auth::id() == $pageowner->id)
    <div class="pt-2 pb-2">

        <div class="card">
            <div class="card-body"><a class="text-muted font-weight-bold" href="#">Change</a></div>
            <div class="card-body border border-bottom"><a class="" href="{{ route('edit.user') }}">Info</a></div>
            <div class="card-body border border-bottom"><a class="" href="{{ route('edit.photo') }}">Foto</a></div>
            <div class="card-body border border-bottom"><a class="" href="{{ route('get.header') }}">Header</a></div>
        </div>


    </div>

@endif