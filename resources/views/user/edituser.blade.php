@extends('template.app')
@section('title','Edit user')

@section('header')
    @include('partial.profileheader')
@endsection


@section('content')
    <div class="card mt-2">
        <div class="cardbody p-3">
            <form action="{{ route('edit.user') }}" method="post" class="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Naam:</label><input type="text" class="form-control" name="name" value="{{ $user->name }}">
                    <span class="text-danger mt-2">{{ $errors->first('name') }}</span>
                </div>
                <div class="form-group">
                    <label for="">Email</label><input type="email" name="email" class="form-control" value="{{$user->email}}" disabled>
                </div>
                <div class="form-group">
                    <label for="">birthdate</label><input type="date" name="birthdate" value="{{$user->profile->birthdate}}">
                    <span class="text-danger mt-2">{{ $errors->first('birthdate') }}</span>
                </div>

                <div class="form-group">
                    <div class="btn-group " data-toggle="">
                        <label for="">Gender:</label>
                        <input type="radio" name="gender" id="option1" autocomplete="off" value="1" {{ $user->profile->gender == 1 ? "checked" : "" }} class="ml-2"> Man


                        <input type="radio" name="gender" id="option2" autocomplete="off" value="0" {{ $user->profile->gender == 0 ? "checked" : "" }} class="ml-2"> Vrouw

                    </div>
                    <span class="text-danger mt-2">{{ $errors->first('gender') }}</span>
                </div>
                <div class="form-group">
                    <label for="">Profielfoto:</label>
                    <input type="file" name="image" class="ml-2">
                </div>
                <div class="form-group d-flex justify-content-end">

                    <button class="btn btn-success">Update</button>

                </div>
            </form>
        </div>
    </div>

@endsection

@section('rightcolumn')
    @include('partial.editMenu');
@endsection

