@extends('template.app')

@section('title','Edit photo')

@section('header')
    @include('partial.profileheader')
@endsection



@section('leftcolumn')

@endsection


@section('content')

    <div class="card mt-2">
        <div class="card-body">
            <h3 class="mt-5">Headers:</h3>
            <div class="card-columns">
                @foreach($headers as $header )
                    <div class="card mt-2">
                        <a href="{{ route('edit.header',$header->id) }}"><img class="card-img-top" src="{{ Storage::url($header->path) }}" alt=""></a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>

@endsection

@section('rightcolumn')
    @include('partial.editMenu')
@endsection