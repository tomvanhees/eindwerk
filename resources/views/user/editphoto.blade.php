@extends('template.app')

@section('title','Edit photo')

@section('header')
    @include('partial.profileheader')
@endsection



@section('leftcolumn')
    <div class="card mt-2">
        <div class="card-body">
            <h3>Nieuwe foto:</h3>
            <form action="{{ route('add.photo') }}" method="post" class="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" name="image">
                <p>
                    <button class="btn btn-success mt-3 ">Toevoegen</button>
                </p>
            </form>
        </div>
    </div>
@endsection


@section('content')

    <div class="card mt-2">
        <div class="card-body">
            @if( count(Auth::user()->profile->profilepicture) != 0)
                <h3>Uw foto´s:</h3>
                <div class="card-columns">
                    @foreach(Auth::user()->profile->profilepicture as $profilepicture )
                        <div class="card" style="max-width: 8rem;">
                            <a href="/edit/photo/{{ $profilepicture->id }}"><img class="card-img-top" src="{{ Storage::url($profilepicture->picturepath) }}" alt="Card image cap"></a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="card mt-2">
                    <div class="card-body text-center">
                        <p class="font-weight-bold">Wij konden nog geen foto van u vinden. </p>
                        <p class="font-weight-bold">Voeg eentje toe of kies een van de onderstaande</p>
                    </div>
                </div>
            @endif


            <h3 class="mt-5">Standaard foto´s:</h3>
            <div class="card-columns">
                @foreach($defaultpictures as $profilepicture )
                    <div class="card mt-2" style="max-width: 8rem;">
                        <a href="/edit/photo/{{ $profilepicture->id }}"><img class="card-img-top" src="{{ Storage::url($profilepicture->picturepath) }}" alt="Card image cap"></a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>





@endsection

@section('rightcolumn')
    @include('partial.editMenu')
@endsection