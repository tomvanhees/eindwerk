@extends('template.app')
@section('title','Edit user')


@section('header')
    @include('partial.profileheader')
@endsection

@section('content')
    <div class="card mt-2">
        <div class="card-body">
            <h1>Deel met de wereld:</h1>
            <form action="/add/article" method="post" class="form" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="content"></textarea>
                </div>

                <div>
                    <input type="file" name="image">
                </div>

                <input type="text" name="tags">
                <div class="form-group d-flex justify-content-end">
                    <button class="btn btn-success">Deel</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('rightcolumn')
    @include('partial.editMenu');
@endsection