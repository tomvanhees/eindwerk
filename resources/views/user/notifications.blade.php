@extends('template.app')
@section('title','Notifications')

@section('header')
    @include('partial.profileheader')
@endsection

@section('content')
    <div class="card mt-2">
        <div class="card-body">
            @if(Auth::user()->unreadNotifications->isEmpty())
                <p class="font-weight-bold text-center">U heeft geen notificaties op dit moment</p>
            @else
                @foreach(Auth::user()->unreadNotifications as $notification)
                    <p>
                        <a href="{{ route('user.page', $notification->data['user_id']) }}">

                            {{ $notification->data['user']['name']}}  </a>{{ $notification->data['sentence'] }}
                        <a href="{{ route('show.article', $notification->data['article_id']) }}">{{ $notification->data['product'] }}</a>
                        {{ $notification->markAsRead() }}
                    </p>

                @endforeach
        </div>
    </div>
    @endif
@endsection
