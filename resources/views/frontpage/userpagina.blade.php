@extends('template.app')
@section('title',$pageowner->name . " ". $pageowner->surname)
@section('header')
    @include('partial.profileheader')
@endsection
@section('leftcolumn')

    <div class="card mt-2">
        <div class="card-body">
            @isset($pageowner->profile->gender)
                <div class=""><span class="font-weight-bold">Geslacht:</span> {{ $pageowner->profile->gender==0 ?"vrouw" : "man" }}</div>
            @endisset
            @isset($pageowner->profile->birthdate)
                <div class=""><span class="font-weight-bold">Geboortedatum:</span> {{ $pageowner->profile->birthdate }}</div>
            @endisset
        </div>
    </div>
    <div class="card mt-2">
        <div class="card-body">
            <div class=" d-flex justify-content-between align-items-center">
                <span class="font-weight-bold">Geïnteresseerd in:</span>
                <span class="badge badge-primary badge-pill">{{ count($pageowner->followings()->get()) }}</span>
            </div>
            <div class=" d-flex justify-content-between align-items-center"><span class="font-weight-bold">Aantal volgers:</span>
                <span class="badge badge-primary badge-pill">{{ count($pageowner->followers()->get()) }}</span>
            </div>

        </div>
    </div>
@endsection

@section('content')


    @foreach($articles as $article)
        @include('partial.article')
    @endforeach
@endsection

@section('rightcolumn')
    @include('partial.editMenu');
@endsection


