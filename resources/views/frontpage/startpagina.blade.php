@extends('template.app')
@section('title','Frontpage')


@section('leftcolumn')

    <div class="mt-2">
        <div class="card">
            <div class="card-body border border-bottom"><a href=" {{ route('user.page',Auth::id()) }}" class="font-weight-bold">
                    <img src="{{ Auth::user()->profile->getProfilePicture() }}"
                         alt="{{ Auth::user()->name .'_'.Auth::user()->surname }}" class="small_icon rounded-circle ml-3">
                    <span class="pl-1 pr-1">{{ Auth::user()->name. ' '. Auth::user()->surname }}</span>
                </a></div>


        </div>
    </div>

@endsection

@section('content')
    @foreach($articles as $article)
        @include('partial.article')
    @endforeach

@endsection

@section('rightcolumn')
    <div class="mt-2">
        <div class="card">
            <div class="card-body text-muted font-weight-bold">Ook actief</div>
            @foreach($users as $follow)
                <div class="card-body border border-bottom">
                    <img src="{{ $follow->profile->getProfilePicture() }}"
                         alt="{{ $follow->name .'_'.$follow->surname }}" class="small_icon rounded-circle ml-3">
                    <a href=" {{ route('user.page',$follow->id) }}">{{ $follow->name. " ". $follow->surname }}</a>
                </div>

            @endforeach
        </div>
    </div>

@endsection

