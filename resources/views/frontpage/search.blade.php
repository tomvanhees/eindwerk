@extends('template.app')
@section('title','search')
@section('header')
    @include('partial.profileheader')
@endsection
@section('content')

    <div class="card mt-2 border border-dark">
        <div class="card-body">
            <div class="container">
                <div class="row ">


                    @foreach($search as $user)
                        <div class="col-sm-1 mt-2"><a href="{{ route('follow',$user->id) }}">{!! Auth::user()->isFollowing($user->id) ? '<i class="fas fa-smile"></i>' : '<i class="far fa-smile"></i>' !!}</a></div>
                        <div class="col-sm-11 mt-1">
                            <img src="{{ $user->profile->getProfilePicture() }}"
                                 alt="{{ $user->name  }}" class="small_icon rounded-circle ml-3">
                            <a href="{{ route('user.page',$user->id) }}">{{$user->name }}</a>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

@endsection