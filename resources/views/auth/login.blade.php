@extends('template.auth')
@section('title','Login')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-sm-4 offset-sm-4 p-5 bg-white rounded mt-5">
                <form action="{{ route('login.post') }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group col-sm-12">
                        <input type="email" class="form-control" placeholder="email" name="email">
                        <span class="text-danger mt-2">{{ $errors->first('email') }}</span>
                    </div>
                    <div class="form-group col-sm-12">
                        <input type="password" class="form-control" placeholder="wachtwoord" name="password">
                        <span class="text-danger mt-2">{{ $errors->first('password') }}</span>
                    </div>
                    <div class="form-group col-sm-12">
                        <button type="submit" class="btn btn-success" value="Register">Login</button>
                    </div>
                    <div class="d-flex justify-content-between mt-3">
                        <a href="{{ route('forgotPassword') }}" class="small">Wachtwoord vergeten?</a><a href="{{ route('register') }}" class="small">registreer</a>
                    </div>
                </form>
                <p class="mt-2 font-weight-bold">Of gebruik deze methoden:</p>
                <div class="form-froup col-md-8 col-sm-12">
                    <a href="{{ route('facebook') }}"><i class="fab fa-facebook text-primary fa-2x"></i> </a>
                </div>
            </div>
        </div>
    </div>
@endsection