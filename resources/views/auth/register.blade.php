@extends('template.auth')
@section('title','Register')
@section('content')
    <div class="row">
        <div class="d-flex justify-content-center align-items-center mt-5">

            <div class="col-sm-6 p-3 bg-white rounded">
                <div class="row">

                    <div class="ml-5">
                        <form action="{{ route('register.post') }}" method="post">
                            {{ csrf_field() }}

                            <h1 class="">Maak een nieuw account:</h1>
                            <div class="form-group col-md-8 col-sm-12">
                                <label for="">naam:</label>
                                <input type="text" class="form-control" placeholder="Voornaam" name="name">
                                <span class="text-danger mt-2">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="form-group col-sm-12 col-md-8">
                                <label for="">Email:</label>
                                <input type="email" class="form-control" placeholder="email" name="email">
                                <span class="text-danger mt-2">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="form-group col-sm-12 col-md-8">
                                <label for="">Wachtwoord:</label>
                                <input type="password" class="form-control" placeholder="wachtwoord" name="password">
                                <span class="text-danger mt-2">{{ $errors->first('password') }}</span>
                            </div>
                            <div class="form-group col-md-8 col-sm-12">
                                <label for="">Geboortedatum:</label>
                                <input type="date" name="birthdate" value=""><br>
                                <span class="text-danger mt-2">{{ $errors->first('birthdate') }}</span>
                            </div>
                            <div class="form-group col-md-8 col-sm-12">
                                <label for="">Geslacht:</label>
                                <div class="btn-group " data-toggle="" name="gender">
                                    <label class="">
                                        <input type="radio" name="gender" id="option1" autocomplete="off" value="1"> Man
                                    </label>
                                    <label class="">
                                        <input type="radio" name="gender" id="option2" autocomplete="off" value="0" class="ml-1"> Vrouw
                                    </label>

                                </div>
                                <br>
                                <span class="text-danger mt-2">{{ $errors->first('gender') }}</span>
                            </div>

                            <div class="formgroup col-md-8 col-sm-12">
                                <p class="text-muted small mt-3">Door op Account maken te klikken, ga je akkoord met onze Algemene voorwaarden en geef je aan dat je ons Gegevensbeleid, inclusief het beleid inzake cookiegebruik, hebt gelezen.</p>
                                <p>
                                    <button type="submit" class="btn btn-success" value="Register">Maak account aan</button>
                                </p>

                            </div>

                        </form>
                        <div class="form-group col-md-8 col-sm-12">
                            <p class="mt-2 font-weight-bold">Maak je account via deze weg:</p>
                            <div class="form-froup col-md-8 col-sm-12">
                                <a href="{{ route('facebook') }}"><i class="fab fa-facebook text-primary fa-2x"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection