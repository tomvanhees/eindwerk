<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//--------------------------AUTH----------------------

//Login
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login')->name('login.post');

//Logout
Route::get('logout','Auth\LoginController@logout')->name('logout');

//Register
Route::get('register','Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register','Auth\RegisterController@register')->name('register.post');

//Facebook
Route::get('login/facebook', 'SocialmediaController@redirectToProvider')->name('facebook');
Route::get('login/facebook/callback', 'SocialmediaController@handleProviderCallback');


//Reset password
Route::get('password','Auth\ForgotPasswordController@showLinkRequestForm')->name('forgotPassword');
Route::post('sendEmail','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('reset', 'Auth\ResetPasswordController@reset')->name('password.request');



//-------------------EDIT----------------------

Route::prefix('edit')->group(function(){

    //edit user
    Route::get('user','ProfileController@user')->name('edit.user');
    Route::post('user','ProfileController@editUser')->name('post.edit.user');

    //edit photo
    Route::get('photo','ProfileController@photo')->name('edit.photo');
    Route::get('photo/{id}','ProfileController@editPhoto');
    Route::post('photo','ProfileController@addPhoto')->name('add.photo');

    //edit header
    Route::get('header','ProfileController@header')->name('get.header');
    Route::get('header/{id}','ProfileController@editHeader')->name('edit.header');
});


//-------------------Articles--------------------------
Route::get('/', 'ArticleController@getAllArticles')->name('home');
Route::get('user/{id}', 'ArticleController@getAllArticlesByUserId')->name('user.page');
Route::get('user/article/{id}','ArticleController@showArticle')->name('show.article');

//add article
Route::get('add/article','Articlecontroller@article')->name('article');
Route::post('add/article','ArticleController@postArticle');


//Add comment
Route::post('add/comment/{article_id}','CommentController@addComment')->name('add.comment');


//Tags
Route::get('tags/{tag}','ArticleController@tag')->name('tag');

//follow user
Route::get('follow/user/{id}','ProfileController@toggleFollowUser')->name('follow');

//Like
Route::get('like/article/{user_id}/{article_id}','ArticleController@toggleLike')->name('like.article');
Route::get('like/comment/{user_id}/{comment_id}/{article_id}','CommentController@toggleLike')->name('like.comment');


//search
Route::get('search', 'SearchController@search')->name('search');


//notifications
Route::get('notifications', 'ProfileController@notifications')->name('notifications');




